# README
## Author:
Adam Case, acase@uoregon.edu
## What is this repository for?
A "getting started" project for CIS 322, introduction to software engineering, at the University of Oregon.

This project provided:
* Experience with GIT workflow with separate configuration: Fork the project, make and test changes locally, commit; turn in configuration file with reference to repo. (Project 0 is practice for this part.)
* Extend a tiny web server in Python, to check understanding of basic web architecture
* Use automated tests to check progress (plus manual tests for good measure)

## Server System Requirements
* Linux (Ubuntu) or MacOS
* Python 3.4 or higher
* Port > 1000, recommended 5000-8000

## Usage
### Running the Server
* `make run` or `start.sh` to start the server.
* While the server is running use `Ctrl + C` to stop the server.
* `make stop` or `stop.sh` to clean up the server process.
### Tests
* Use the `tests.sh` script under "tests" folder to test the expected outcomes.
